![build_status](https://gitlab.com/apiazza134/org-mq-paffuti/badges/master/pipeline.svg)

# Org file di studio per Meccanica Quantistica (Paffuti)
È disponibile l'export dell'org file in
- [pdf](https://gitlab.com/apiazza134/org-mq-paffuti/-/jobs/artifacts/master/raw/paffuti.pdf?job=org_export)
- [html](https://gitlab.com/apiazza134/org-mq-paffuti/-/jobs/artifacts/master/raw/paffuti.html?job=org_export)
